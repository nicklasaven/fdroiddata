Categories:Navigation
License:GPL-3.0
Author Name:Nicklas Avén
Author Email:nicklas.aven@jordogskog.no
Web Site:https://github.com/TilelessMap/TilelessMap
Source Code:https://github.com/TilelessMap/TilelessMap
Issue Tracker:https://github.com/TilelessMap/TilelessMap/issues

Name:TilelessMap

Repo Type:git
Repo:https://github.com/TilelessMap/TilelessMap

Build:0.1.1,1
    commit=v0.1.0
    subdir=Android/app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.1.0
Current Version Code:1


